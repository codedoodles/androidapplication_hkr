package com.example.cv_application.ui.gitlab;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.cv_application.R;
import com.example.cv_application.databinding.FragmentGitlabBinding;


public class GitlabFragment extends Fragment {

    private GitlabViewModel gitLabViewModel;
    private FragmentGitlabBinding binding;
    private WebView myWebView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        gitLabViewModel =
                new ViewModelProvider(this).get(GitlabViewModel.class);

        binding = FragmentGitlabBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        myWebView = binding.webview;
        View v=inflater.inflate(R.layout.fragment_gitlab, container, false);
        myWebView = (WebView) v.findViewById(R.id.webview);

        // Enable Javascript and internet access
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.getSettings().setAllowContentAccess(true);
        myWebView.getSettings().setAllowFileAccess(true);

        // Force links and redirects to open in the WebView instead of in a browser
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl("https://gitlab.com/codedoodles");
        myWebView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == MotionEvent.ACTION_UP
                        && myWebView.canGoBack()) {
                    myWebView.goBack();
                    return true;
                }
                return false;
            }
        });

        return v;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}