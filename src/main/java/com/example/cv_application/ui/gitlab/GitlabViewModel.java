package com.example.cv_application.ui.gitlab;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GitlabViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public GitlabViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is github fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}