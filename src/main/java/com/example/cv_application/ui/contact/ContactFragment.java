package com.example.cv_application.ui.contact;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cv_application.R;

public class ContactFragment extends Fragment {

    private ContactViewModel mViewModel;
    private String messageSubject = "Contact regarding your CV App";
    private String recipient = "emailme@gmail.com";
    private String phoneNumber = "07312345678";

    public static ContactFragment newInstance() {
        return new ContactFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);

        // Clickable mail address, mail icon
        TextView mailAddress = (TextView) view.findViewById(R.id.email_address);
        mailAddress.setOnClickListener(this::onClick);
        ImageView mailIcon = (ImageView) view.findViewById(R.id.mail_icon);
        mailIcon.setOnClickListener(this::onClick);

        // Clickable phone number, phone number icon
        TextView phoneNumber = (TextView) view.findViewById(R.id.phone_number);
        phoneNumber.setOnClickListener(this::call);
        ImageView phoneIcon = (ImageView) view.findViewById(R.id.phone_icon);
        phoneIcon.setOnClickListener(this::call);

        return view;
    }

    public void onClick(View view){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{recipient});
        intent.putExtra(Intent.EXTRA_SUBJECT, messageSubject);

        intent.setType("message/rfc822");

        startActivity(Intent.createChooser(intent, "Choose an email client"));
    }

    public void call(View view){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:"+phoneNumber));
        startActivity(callIntent);
    }
}